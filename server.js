//instancia de express
var app = require('express')();
//crea el medio de transimision
var http = require('http').Server(app);
//se crea a travez del servidor
var io = require('socket.io')(http);

io.on('connection',function(socket) {
	// body...
	console.log('user conectado');
	socket.on('mensaje',function(mensaje){
		// socket.emit("mensaje","tengo hambre");
		socket.broadcast.emit("mensaje",mensaje);

	});
});

//levantar el servidor
http.listen(9000, function(){
	console.log('servidor corriendo en localhost');
});